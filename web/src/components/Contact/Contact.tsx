    import './Contact.css'
    import {MouseEventHandler} from "react";

    interface IContact {
        image: string
        contactName: string
        onClickHandler: MouseEventHandler<HTMLDivElement>
    }

    const Contact = (props: IContact) => {
        return <div className='contact' onClick={props.onClickHandler}>
            <div className='width'>
            <img src={props.image}
                alt="contact_image"
                className='image_contact'
                onError={({ currentTarget }) => {
                    currentTarget.onerror = null;
                    currentTarget.src="https://thumbs.dreamstime.com/b/no-thumbnail-images-placeholder-forums-blogs-websites-148010338.jpg";
                }}
            />
            <p className='contact_title'>{props.contactName}</p>
            </div>
        </div>
    };

    export default Contact;