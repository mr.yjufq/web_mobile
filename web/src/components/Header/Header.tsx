import './Header.css'
import {NavLink} from "react-router-dom";

const Header = () => {
    return (
        <header className='header'>
            <nav className='nav'>
                <ul className='nav_menu'>
                    <li className='li_item'><NavLink to='/contacts'  className='link'>Contacts</NavLink></li>
                    <li className='li_item'><NavLink to='/add-contacts' className='link'>Add new contact</NavLink></li>
                </ul>
            </nav>
        </header>
    )
};

export default Header;