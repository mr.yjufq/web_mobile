import './Modal.css'
import Backdrop from "../Backdrop/Backdrop";
import {MouseEventHandler} from "react";

interface IModal {
    show: boolean
    onCloseHandler: MouseEventHandler
    deleteHandler: MouseEventHandler<HTMLButtonElement>
    editHandler: MouseEventHandler<HTMLButtonElement>
    image: string
    name: string
    email: string
    phone: string
}

const Modal = (props: IModal) => {

    return (
        <>
            <div
                className='Modal'
                style={{
                    transform: props.show ? 'translateY(0)' : 'translateY(-150vh)',
                    opacity: props.show ? '1' : '0'
                }}
            >
                <button onClick={props.onCloseHandler} className='close'>X</button>
                <div className='contact_info'>
                    <img src={props.image} alt="contact_image" className='image_contact'/>
                    <div>
                        <p>{props.name}</p>
                        <p>E-mail: <a href={`mailto:${props.email}`} className='link_modal'>{props.email}</a></p>
                        <p>Phone: <a href={`tel:${props.phone}`} className='link_modal'>{props.phone}</a></p>
                    </div>
                </div>
                <div className='buttons'>
                    <button onClick={props.deleteHandler} className='button_modal  delete'>Delete</button>
                    <button className='button_modal' onClick={props.editHandler}>Edit</button>
                </div>
            </div>
            <Backdrop show={props.show} CloseHandler={props.onCloseHandler}/>
        </>
    );
};

export default Modal;