import {FormEventHandler, MouseEventHandler} from 'react';
import './Form.css'

interface IForm {
    onSubmitHandler: FormEventHandler<HTMLFormElement>
    onClickHandler: MouseEventHandler<HTMLButtonElement>
    onInputHandler: FormEventHandler<HTMLInputElement>
    image: string
    valueName: string
    valuePhone: string
    valueEmail: string
    valuePhoto: string
    nameForm: string
}

const Form = (props: IForm) => {
    return <form className='form' onSubmit={props.onSubmitHandler}>
        <p className='title_form'>{props.nameForm}</p>
        <label className='label'>Name</label>
        <input type="text" placeholder='Name' name='name' onInput={props.onInputHandler} value={props.valueName} className='input'/>
        <label className='label'>Phone</label>
        <input type="tel" placeholder='Phone' name='phone' onInput={props.onInputHandler} value={props.valuePhone} className='input'/>
        <label className='label'>E-mail</label>
        <input type="email" placeholder='E-mail' name='email' onInput={props.onInputHandler} value={props.valueEmail} className='input'/>
        <label className='label'>URL photo</label>
        <input type="url" placeholder='Photo' name='photo' onInput={props.onInputHandler} value={props.valuePhoto} className='input'/>
        <p className='title_preview'>Photo preview</p>
        <img src={props.image === '' ? "https://thumbs.dreamstime.com/b/no-thumbnail-images-placeholder-forums-blogs-websites-148010338.jpg" : props.image}
             alt="contact_photo"
             className='image_preview'
             onError={({ currentTarget }) => {
                 currentTarget.onerror = null;
                 currentTarget.src="https://thumbs.dreamstime.com/b/no-thumbnail-images-placeholder-forums-blogs-websites-148010338.jpg";
             }}
        />
        <button className='button_form'>Save</button>
        <button onClick={props.onClickHandler} className='button_form'>Back to contacts</button>
    </form>
};

export default Form;