import './Contacts.css'
import {useDispatch, useSelector} from "react-redux";
import {AppDispatch, RootState} from "../../index";
import {useEffect} from "react";
import Loader from "../../components/UI/Loader/Loader";
import Modal from "../../components/UI/Modal/Modal";
import Contact from "../../components/Contact/Contact";
import {useNavigate} from "react-router-dom";
import {changeInfo, fetchContacts, fetchDeleteContact, showInfo} from "../../store/contactSlice";

interface IContacts {
    name: string
    phone: string
    email: string
    photo: string
    key?: string
}

const Contacts = () => {
    const contacts = useSelector<RootState, IContacts[]>(state => state.contacts.contacts);
    const loader = useSelector<RootState, boolean>(state => state.contacts.isLoading);
    const info = useSelector<RootState, boolean>(state => state.contacts.showInfo);
    const infoModal = useSelector<RootState, IContacts>(state => state.contacts.info);

    const dispatch = useDispatch<AppDispatch>();

    const navigate = useNavigate();

    const onClickHandler = (key: string | undefined) => {
        const index = contacts.findIndex(element => element.key === key);

        dispatch(changeInfo(contacts[index]));
        dispatch(showInfo(true));
    };

    const onDeleteHandler = () => {
        if (infoModal.key) {
            dispatch(fetchDeleteContact(infoModal.key));
        }

        dispatch(showInfo(false));
    };

    useEffect(() => {
        dispatch(fetchContacts());
    }, [dispatch]);

    const renderContacts = contacts.map(element => {
        return <Contact
            image={element.photo}
            contactName={element.name}
            key={element.key}
            onClickHandler={() => onClickHandler(element.key)}
        />
    });

    const EditContact = (key: string | undefined) => {
        navigate('/contacts/' + key + '/edit');
        dispatch(showInfo(false));
    };

    return loader ? <Loader show={loader}/> :
        <div className='contacts'>
            {info ? <Modal
                editHandler={() => EditContact(infoModal.key)}
                deleteHandler={onDeleteHandler}
                show={info}
                onCloseHandler={() => dispatch(showInfo(false))}
                name={infoModal.name}
                phone={infoModal.phone}
                image={infoModal.photo}
                email={infoModal.email}
            /> : null}
            {renderContacts}
        </div>
};

export default Contacts;