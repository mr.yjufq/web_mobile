import {useNavigate, useParams} from "react-router-dom";
import Form from "../../components/Form/Form";
import {useDispatch, useSelector} from "react-redux";
import {AppDispatch, RootState} from "../../index";
import React, {useState} from "react";
import {fetchPutContact} from "../../store/contactSlice";
import Loader from "../../components/UI/Loader/Loader";

interface IContacts {
    name: string
    phone: string
    email: string
    photo: string
    key?: string | undefined
}

interface IContact {
    name: string
    phone: string
    email: string
    photo: string
}

const EditContacts = () => {
    const {id} = useParams();
    const navigate = useNavigate();

    const info = useSelector<RootState, IContacts>(state => state.contacts.info);
    const loader = useSelector<RootState, boolean>(state => state.contacts.isLoading);

    const dispatch = useDispatch<AppDispatch>();

    const [editContact, setEditContact] = useState<IContact>({
        name: info.name,
        phone: info.phone,
        email: info.email,
        photo: info.photo
    });

    const onInputHandler = (event: React.FormEvent<HTMLInputElement>) => {
        const {name, value} = event.currentTarget;

        setEditContact((prevState: IContact) => {
            return {
                ...prevState,
                [name]: value,
            };
        });
    };

    const onSubmitHandler = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (editContact.name.trim() === '' || editContact.email.trim() === '' || editContact.phone === '' || editContact.photo === '') {
            return
        } else {
            await dispatch(fetchPutContact({key: id, newContact: editContact}));

            setEditContact({
                name: '',
                phone: '',
                email: '',
                photo: ''
            });
        }

        navigate('/contacts');
    };

    const onCLickHandler = () => {
        navigate('/contacts');
    }

    return  loader ? <Loader show={loader}/> :<Form
        nameForm='Edit contact'
        valueEmail={editContact.email}
        valueName={editContact.name}
        valuePhone={editContact.phone}
        valuePhoto={editContact.photo}
        onInputHandler={event => onInputHandler(event)}
        onSubmitHandler={event => onSubmitHandler(event)}
        onClickHandler={onCLickHandler}
        image={editContact.photo}
    />

};

export default EditContacts;