import React, {useState} from "react";
import Form from "../../components/Form/Form";
import {AppDispatch, RootState} from "../../index";
import {useDispatch, useSelector} from "react-redux";
import {fetchPostContact} from "../../store/contactSlice";
import {useNavigate} from "react-router-dom";
import Loader from "../../components/UI/Loader/Loader";

interface IContact {
    name: string
    phone: string
    email: string
    photo: string
}

const AddContacts = () => {
    const [newContact, setNewContact] = useState<IContact>({
        name: '',
        phone: '',
        email: '',
        photo: ''
    });

    const loader = useSelector<RootState, boolean>(state => state.contacts.isLoading);

    const dispatch = useDispatch<AppDispatch>();

    const navigate = useNavigate();

    const onInputHandler = (event: React.FormEvent<HTMLInputElement>) => {
        const {name, value} = event.currentTarget;

        setNewContact((prevState: IContact) => {
            return {
                ...prevState,
                [name]: value,
            };
        });
    };

    const onSubmitHandler = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (newContact.name.trim() === '' || newContact.email.trim() === '' || newContact.phone === '' || newContact.photo === '') {
            return
        } else {
            await dispatch(fetchPostContact(newContact));

            setNewContact({
                name: '',
                phone: '',
                email: '',
                photo: ''
            });
        }
    };

    const onCLickHandler = () => {
        navigate('/contacts');
    }

    return loader ? <Loader show={loader}/> : <Form
        nameForm='Add contact'
        valueEmail={newContact.email}
        valueName={newContact.name}
        valuePhone={newContact.phone}
        valuePhoto={newContact.photo}
        onInputHandler={event => onInputHandler(event)}
        onSubmitHandler={event => onSubmitHandler(event)}
        onClickHandler={onCLickHandler}
        image={newContact.photo}
    />
};

export default AddContacts;