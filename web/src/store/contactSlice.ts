import {createAsyncThunk, createSlice, PayloadAction} from "@reduxjs/toolkit";
import instance from "../API/createAxios";
import {AxiosRequestConfig, AxiosResponse} from "axios";

interface IContacts {
    name: string
    phone: string
    email: string
    photo: string
    key?: string | undefined
}

interface IState {
    contacts: IContacts[]
    isLoading: boolean
    error: Error | null
    showInfo: boolean
    info: IContacts
}

const initialState: IState = {
    contacts: [],
    isLoading: false,
    error: null,
    showInfo: false,
    info : {
        name: '',
        phone: '',
        email: '',
        photo: '',
        key: ''
    }
}

export const fetchContacts = createAsyncThunk(
    'contacts.get',
    async () => {
        const res = await instance.get<AxiosRequestConfig, AxiosResponse>('contacts.json');
        if (res !== null) {
            const data = Object.keys(res.data).map(key => {
                return {key: key, ...res.data[key]}
            });

            return data;
        } else {
            return [];
        }
    }
);

export const fetchPostContact = createAsyncThunk(
    'contacts.post',
    async (payload: IContacts) => {
        await instance.post<AxiosRequestConfig, AxiosResponse>('contacts.json', payload);
    }
);

export const fetchDeleteContact = createAsyncThunk(
    'contacts.delete',
    async (payload: string, {dispatch}) => {
        await instance.delete<AxiosRequestConfig, AxiosResponse>('contacts/' + payload + '.json');
        await dispatch(fetchContacts());
    }
);

export const fetchPutContact = createAsyncThunk(
    'contacts.put',
    async (payload: {key: string | undefined, newContact: IContacts}) => {
        await instance.put<AxiosRequestConfig, AxiosResponse>('contacts/' + payload.key + '.json', payload.newContact);
    }
);


const ContactSlice = createSlice({
    name: 'contacts',
    initialState,
    reducers : {
        showInfo: (state, action:PayloadAction<boolean>) => {
            state.showInfo = action.payload;
        },
        changeInfo : (state, action: PayloadAction<IContacts>) => {
            state.info = action.payload;
        }
    },
    extraReducers : (builder) => {
        builder
            .addCase(fetchContacts.pending, (state) => {
                state.isLoading = true;
            })
            .addCase(fetchContacts.fulfilled, (state, action) => {
                state.contacts = action.payload;
                state.isLoading = false;
                state.error = null;
            })
            .addCase(fetchContacts.rejected, (state, action) => {
                state.isLoading = false;
                state.error = action.error as Error;
            })
            .addCase(fetchPostContact.pending, (state) => {
                state.isLoading = true;
            })
            .addCase(fetchPostContact.fulfilled, (state) => {
                state.isLoading = false;
                state.error = null;
            })
            .addCase(fetchPostContact.rejected, (state, action) => {
                state.isLoading = false;
                state.error = action.error as Error;
            })
            .addCase(fetchDeleteContact.pending, (state) => {
                state.isLoading = true;
            })
            .addCase(fetchDeleteContact.fulfilled, (state) => {
                state.isLoading = false;
                state.error = null;
            })
            .addCase(fetchDeleteContact.rejected, (state, action) => {
                state.isLoading = false;
                state.error = action.error as Error;
            })
            .addCase(fetchPutContact.pending, (state) => {
                state.isLoading = true;
            })
            .addCase(fetchPutContact.fulfilled, (state) => {
                state.isLoading = false;
                state.error = null;
            })
            .addCase(fetchPutContact.rejected, (state, action) => {
                state.isLoading = false;
                state.error = action.error as Error;
            })
    }
});

export const {
    showInfo,
    changeInfo,

} = ContactSlice.actions;
export default ContactSlice.reducer;
