import React from 'react';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import AddContact from "./containers/AddContact/AddContact";
import Layout from "./components/Layout/Layout";
import Contacts from "./containers/Contacts/Contacts";
import EditContact from "./containers/EditContacts/EditContact";


function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path='/' element={<Layout/>}>
                    <Route index element={<Contacts/>}/>
                    <Route path='add-contacts' element={<AddContact/>}/>
                    <Route path='contacts' element={<Contacts/>}/>
                    <Route path='contacts/:id/edit' element={<EditContact/>}/>
                </Route>
            </Routes>
        </BrowserRouter>
    );
}

export default App;
