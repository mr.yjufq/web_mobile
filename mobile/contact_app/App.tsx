import {Provider} from "react-redux";
import {configureStore} from "@reduxjs/toolkit";
import contactReducer from "./src/store/contactSlices";
import ContactsList from "./src/components/Contacts/ContactsList";

const store = configureStore({
    reducer: {
        contacts: contactReducer
    }
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default function App() {
    return (
        <Provider store={store}>
            <ContactsList/>
        </Provider>
    );
}
