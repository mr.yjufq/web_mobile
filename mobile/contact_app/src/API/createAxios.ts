import axios from "axios";

const instance = axios.create({
    baseURL: "https://exam-9-ca3e0-default-rtdb.asia-southeast1.firebasedatabase.app/"
});

export default instance;