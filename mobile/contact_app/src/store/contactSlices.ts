import {createSlice, createAsyncThunk, PayloadAction} from "@reduxjs/toolkit";
import instance from "../API/createAxios";
import {AxiosRequestConfig, AxiosResponse} from "axios";

interface IContacts {
    name: string
    phone: string
    email: string
    photo: string
    key?: string | undefined
}

interface IState {
    contacts: IContacts[],
    info : IContacts
    show: boolean
}

const initialState: IState = {
    contacts: [],
    info : {
        name: '',
        phone: '',
        email: '',
        photo: '',
        key: ''
    },
    show: false
};

export const getContacts = createAsyncThunk(
    'GET',
    async () => {
        const res = await instance.get<AxiosRequestConfig, AxiosResponse>('contacts.json');
        if (res !== null) {
            const data = Object.keys(res.data).map(key => {
                return {key: key, ...res.data[key]}
            });

            return data;
        } else {
            return [];
        }
    }
);

const contactSlice = createSlice({
    name: 'contacts',
    initialState,
    reducers: {
        showInfo: (state, action:PayloadAction<boolean>) => {
            state.show = action.payload;
        },
        changeInfo : (state, action: PayloadAction<IContacts>) => {
            state.info = action.payload;
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(getContacts.fulfilled, (state, action) => {
                state.contacts = action.payload;
            })
    }
});

export const {
    showInfo,
    changeInfo
} = contactSlice.actions;

export default contactSlice.reducer;