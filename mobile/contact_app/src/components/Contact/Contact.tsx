import {GestureResponderEvent, Image, Text, TouchableHighlight, View} from "react-native";
import styles from "./Contact.css";

interface IPost {
    uri: string
    name: string
    phone: string
    email: string
    onCLickHandler: (event: GestureResponderEvent) => void
}

const Contact = (props: IPost) => {

    return <TouchableHighlight onPress={props.onCLickHandler}>
        <View style={styles.contact}>
            <Image
                style={styles.image}
                source={{uri: props.uri}}
            />
            <View style={styles.titles}>
                <Text style={styles.title}>Name: {props.name}</Text>
                <Text style={styles.title}>Phone: {props.phone}</Text>
                <Text style={styles.title}>E-mail: {props.email}</Text>
            </View>
        </View>
    </TouchableHighlight>;
};

export default Contact;