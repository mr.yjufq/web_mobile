import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    contact: {
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        padding: 20,
        backgroundColor: '#7B9AAA',
        marginTop: 10,
        width: 340,
        height: 300,
        borderRadius: 20,
    },
    image: {
        height: 120,
        width: 120,
        borderRadius: 10,
        marginBottom: 20,
    },
    title: {
        fontSize: 20,
        width: 300,
        marginLeft: 25,
        color: 'whitesmoke',
        fontWeight: 'bold'
    },
    titles: {
        width: 300,
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
    }
});

export default styles;
