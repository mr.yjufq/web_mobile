import {Alert, FlatList, Image, Modal, Pressable, Text, View} from "react-native";
import {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import Contact from "../Contact/Contact";
import {AppDispatch, RootState} from "../../../App";
import {getContacts, showInfo, changeInfo} from "../../store/contactSlices";
import styles from "./ContactList.css";


interface IContact {
    name: string
    phone: string
    email: string
    photo: string
    key?: string | undefined
}

const ContactList = () => {
    const contacts = useSelector<RootState, IContact[]>((state) => state.contacts.contacts);
    const info = useSelector<RootState, IContact>((state) => state.contacts.info);
    const show = useSelector<RootState, boolean>((state) => state.contacts.show);

    const dispatch = useDispatch<AppDispatch>();

    const onClickHandler = (key: string | undefined) => {
        const index = contacts.findIndex(element => element.key === key);

        dispatch(changeInfo(contacts[index]));
        dispatch(showInfo(true));
    };

    useEffect(() => {
        dispatch(getContacts())
    }, [dispatch])

    return <View style={styles.contacts}>
        <FlatList
            data={contacts}
            renderItem={({item}) => <Contact
                name={item.name}
                email={item.email}
                phone={item.phone}
                uri={item.photo}
                onCLickHandler={() => onClickHandler(item.key)}
            />}
            keyExtractor={item => item.email}
            ListEmptyComponent={<Text>Empty List</Text>}
        />
        {show ?  <View style={styles.centeredView}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={show}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                    dispatch(showInfo(false));
                }}>
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Image
                            style={styles.image}
                            source={{uri: info.photo}}
                        />
                        <Text style={styles.modalText}>Name: {info.name}</Text>
                        <Text style={styles.modalText}>E-Mail: {info.email}</Text>
                        <Text style={styles.modalText}>Phone: {info.phone}</Text>
                        <Pressable
                            style={[styles.button, styles.buttonClose]}
                            onPress={() => dispatch(showInfo(false))}>
                            <Text style={styles.textStyle}>Back to List</Text>
                        </Pressable>
                    </View>
                </View>
            </Modal>
            <Pressable
                style={[styles.button, styles.buttonOpen]}
                onPress={() => dispatch(showInfo(true))}>
                <Text style={styles.textStyle}>Show Modal</Text>
            </Pressable>
        </View> : null}
    </View>
};

export default ContactList;